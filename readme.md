## Processes killer

The utility allows for killing procces by given name. It runs the timer checking process timelife at specified time. 

Run console application with following parameters:  

- Process name (string).
- Lifetime of the process (integer).
- Time period which timer uses for lifetime checks(integer).

If you want to check kill notepad process in 5 minutes from its start time and check every 2 minutes how long it lives use the fillowing command: 
``` c
ProcessKiller.exe notepad 5 2
```