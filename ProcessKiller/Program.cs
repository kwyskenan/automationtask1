﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessKiller
{
    class Program
    {
        static void Main(string[] args)
        {
            string processName = args[0];
           

            if (args == null || args.Length != 3 || !int.TryParse(args[1], out int allowedLifeTime) || !int.TryParse(args[2], out int checkTime))
            {
                Console.WriteLine("Incorrect arguments! Try again.");
                return;
            }
            var startTime = TimeSpan.Zero;
            TimeSpan periodTime = TimeSpan.FromMinutes(checkTime);

            var timer = new System.Threading.Timer((e) => {
                List<Process> activeProcesses = new List<Process>(Process.GetProcessesByName(processName));
                foreach (Process particularProcess in activeProcesses)
                {
                    int lifeCycle = CheckLifeTime(particularProcess);
                    if (lifeCycle >= allowedLifeTime)
                    {
                        particularProcess.Kill(); 
                    }                    
                }
            }, null, startTime, periodTime);
           
            Console.ReadLine();
        }

        static int CheckLifeTime(Process process)
        {
            var startTime = process.StartTime;
            DateTime actualTime = DateTime.Now;
            int timeDiff = (int)((actualTime - startTime).Minutes);
            return timeDiff;
        }

    }
}
